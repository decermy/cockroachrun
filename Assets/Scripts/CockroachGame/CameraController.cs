using System;
using ServiceLocator;
using UnityEngine;

namespace CockroachGame
{
    public class CameraController : IInitializable, IUpdatable
    {
        [Serializable]
        public class Settings
        {
            [Range(5, 15f)]
            public float CameraDistance = 10;
        }
        
        private Camera _camera;
        private GameConfig _gameConfig;

        public void Initialize()
        {
            _camera = ServiceLocator<Camera>.Get();
            _gameConfig = ServiceLocator<GameConfig>.Get();
        }

        public void Update()
        {
            Vector3 cameraPosition = _camera.transform.position;
            float cameraDistance = _gameConfig.CameraControllerSettings.CameraDistance;

            _camera.transform.position = new Vector3(cameraPosition.x, cameraPosition.y, -cameraDistance);
        }
    }
}