using System;
using CockroachGame.ComponentEntities;
using CockroachGame.GameObjectsManager;
using ServiceLocator;
using UnityEngine;

namespace CockroachGame.Cockroach
{
    public class CockroachController : IInitializable, IUpdatable
    {
        [Serializable]
        public class Settings
        {
            public float DirectionChangeAngleSpeed = 1;
            public float MoveSpeed = 1f;
            public float RotationAngleSpeed = 1f;
        }

        private CockroachComponent _cockroachComponent;
        private StartZoneComponent _startZoneComponent;
        private FinishZoneComponent _finishZoneComponent;
        private DangerZoneComponent _dangerZoneComponent;
        private GameConfig _gameConfig;
        private CockroachMovement _cockroachMovement;

        private Vector3 _lastMoveDirection;

        private Vector3 _forwardV3 = Vector3.forward;

        public void Initialize()
        {
            _cockroachComponent = ServiceLocator<IGameObjectManager>.Get().CockroachComponent;
            _dangerZoneComponent = ServiceLocator<IGameObjectManager>.Get().DangerZoneComponent;

            _cockroachMovement = ServiceLocator<CockroachMovement>.Get();

            _gameConfig = ServiceLocator<GameConfig>.Get();

            _startZoneComponent = ServiceLocator<StartZoneComponent>.Get();
            _finishZoneComponent = ServiceLocator<FinishZoneComponent>.Get();

            Reset();
        }

        public void Update()
        {
            CalculateMoveDirection(out Vector3 finalMoveDirection);
            
            MoveCockroach(finalMoveDirection);

            _lastMoveDirection = finalMoveDirection;

            CalculateRotation(finalMoveDirection, out Quaternion cockroachAdditionalRotation);
            
            RotateCockroach(cockroachAdditionalRotation);
        }

        private void CalculateMoveDirection(out Vector3 finalMoveDirection)
        {
            Vector3 cockroachPosition = _cockroachComponent.transform.position;

            Vector3 moveDirectionNormalized = _cockroachMovement.GetMoveDirectionNormalized(cockroachPosition,
                _finishZoneComponent.transform.position);

            Vector3 currentMoveDirection;

            float gameConfigDangerZoneRange = _gameConfig.DungerZoneSettings.DangerZoneRange;

            if (_cockroachMovement.TryGetFearDirection(cockroachPosition,
                _dangerZoneComponent.transform.position, gameConfigDangerZoneRange, out Vector3 fearDirectionPart,
                out float fearDirectionCoef))
            {
                currentMoveDirection = _cockroachMovement.GetFearedMoveDirection(moveDirectionNormalized,
                    fearDirectionPart, fearDirectionCoef);
            }
            else
            {
                currentMoveDirection = moveDirectionNormalized;
            }

            float directionChangeAngleSpeed = _gameConfig.CockroachControllerSettings.DirectionChangeAngleSpeed;

            finalMoveDirection = _cockroachMovement.GetToward2DRotatedVector(_lastMoveDirection,
                currentMoveDirection, directionChangeAngleSpeed * Time.deltaTime);
        }
        
        private void MoveCockroach(Vector3 finalMoveDirection)
        {
            _cockroachComponent.transform.position +=
                finalMoveDirection * _gameConfig.CockroachControllerSettings.MoveSpeed * Time.deltaTime;
        }
        
        private void CalculateRotation(Vector3 finalMoveDirection, out Quaternion cockroachAdditionalRotation)
        {
            Vector3 cockroachHeadDirection = _cockroachComponent.HeadDirection.forward;
            cockroachHeadDirection.z = 0;

            Vector3 cockroachPosition = _cockroachComponent.transform.position;
            Debug.DrawRay(cockroachPosition, finalMoveDirection, Color.green);
            Debug.DrawRay(cockroachPosition, cockroachHeadDirection, Color.white);

            float rotationAngleSpeed = _gameConfig.CockroachControllerSettings.RotationAngleSpeed;

            cockroachAdditionalRotation = _cockroachMovement.GetToward2DRotation(
                cockroachHeadDirection,
                finalMoveDirection, Time.deltaTime * rotationAngleSpeed);
        }

        private void RotateCockroach(Quaternion cockroachAdditionalRotation)
        {
            _cockroachComponent.transform.rotation =
                cockroachAdditionalRotation * _cockroachComponent.transform.rotation;
        }

        public void Reset()
        {
            Vector3 cockroachStartPosition = _startZoneComponent.transform.position;
            cockroachStartPosition.z = 0;
            _cockroachComponent.transform.position = cockroachStartPosition;
        }
    }
}