using UnityEngine;

namespace CockroachGame.Cockroach
{
    public class CockroachMovement
    {
        private Vector3 _zeroV3 = Vector3.zero;
        private Vector3 _forwardV3 = Vector3.forward;

        public Vector3 GetFearedMoveDirection(Vector3 moveDirectionNormalized, Vector3 fearDirection,
            float fearDirectionCoef)
        {
            Vector3 moveDirectionToFearDirectionProject =
                Vector3.Project(moveDirectionNormalized, fearDirection);

            Vector3 fearDirectionPart = fearDirection.normalized * fearDirectionCoef;
            Vector3 moveDirectionPart;

            float moveDirectionCoef = (1 - fearDirectionCoef);
            if (Vector3.Dot(moveDirectionToFearDirectionProject, fearDirection) >= 0)
            {
                moveDirectionPart = moveDirectionNormalized * moveDirectionCoef;
            }
            else
            {
                Vector3 unfearedSideMoveDirection = moveDirectionNormalized - moveDirectionToFearDirectionProject;

                Vector3 unfearedSideMoveDirectionNormalized = unfearedSideMoveDirection.normalized;

                moveDirectionPart = unfearedSideMoveDirectionNormalized * moveDirectionCoef;
            }

            return (moveDirectionPart + fearDirectionPart).normalized;
        }

        public bool TryGetFearDirection(Vector3 characterPosition, Vector3 fearSourcePosition, float fearRange,
            out Vector3 fearDirection, out float fearDirectionCoef)
        {
            fearDirection = _zeroV3;
            fearDirectionCoef = 0;

            if (fearRange == 0)
            {
                return false;
            }

            Vector3 fearTargetVector = characterPosition - fearSourcePosition;
            fearTargetVector.z = 0;

            if (fearTargetVector.sqrMagnitude > fearRange * fearRange)
            {
                return false;
            }

            float fearTargetVectorMagnitude = fearTargetVector.magnitude;
            float fearTargetWeight = 1 - Mathf.Clamp01(fearTargetVectorMagnitude / fearRange);

            fearDirection = fearTargetVector.normalized * fearTargetWeight;
            fearDirectionCoef = fearTargetWeight;

            return true;
        }

        public Vector3 GetMoveDirectionNormalized(Vector3 characterPosition, Vector3 targetPosition)
        {
            Vector3 moveDirection = (targetPosition - characterPosition);
            moveDirection.z = 0;
            return moveDirection.normalized;
        }

        public Vector3 GetToward2DRotatedVector(Vector3 source, Vector3 target, float rotationAngle)
        {
            if (source == Vector3.zero)
            {
                return target;
            }

            return GetToward2DRotation(source, target, rotationAngle) * source;
        }

        public Quaternion GetToward2DRotation(Vector3 source, Vector3 target, float rotationAngle)
        {
            float fullAngle = Vector3.Angle(source, target);

            float directionCoef = 1;

            float rotationCrossProductZ = Vector3.Cross(source, target).z;
            if (rotationCrossProductZ < 0)
            {
                directionCoef = -directionCoef;
            }

            if (rotationAngle > fullAngle)
            {
                rotationAngle = fullAngle;
            }

            return Quaternion.AngleAxis(rotationAngle, directionCoef * _forwardV3);
        }
    }
}