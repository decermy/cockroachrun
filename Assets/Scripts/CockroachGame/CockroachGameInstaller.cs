using CockroachGame.Cockroach;
using CockroachGame.ComponentEntities;
using CockroachGame.Game;
using CockroachGame.GameObjectsManager;
using CockroachGame.Input;
using ServiceLocator;
using UnityEngine;

namespace CockroachGame
{
    public class CockroachGameInstaller : BaseInstaller
    {
        [SerializeField]
        private Camera _camera;

        [SerializeField]
        private PrefabsConfig _prefabsConfig;

        [SerializeField]
        private GameConfig _gameConfig;

        [SerializeField]
        private StartZoneComponent _startZoneComponent;

        [SerializeField]
        private FinishZoneComponent _finishZoneComponent;

        [SerializeField]
        private EndGameScreenComponent _endGameScreenComponent;

        protected override void InstallServices()
        {
            ServiceLocator<Camera>.Register(_camera);
            ServiceLocator<StartZoneComponent>.Register(_startZoneComponent);
            ServiceLocator<FinishZoneComponent>.Register(_finishZoneComponent);

            ServiceLocator<EndGameScreenComponent>.Register(_endGameScreenComponent);

            ServiceLocator<PrefabsConfig>.Register(_prefabsConfig);
            ServiceLocator<GameConfig>.Register(_gameConfig);

            ServiceLocator<CameraController>.Register(new CameraController());

            ServiceLocator<GameObjectFactory>.Register(new GameObjectFactory());
            ServiceLocator<IGameObjectManager>.Register(new GameObjectManager());

            ServiceLocator<CockroachMovement>.Register(new CockroachMovement());
            ServiceLocator<CockroachController>.Register(new CockroachController());

            ServiceLocator<IInputManager>.Register(new InputManager());

            ServiceLocator<DungerZoneController>.Register(new DungerZoneController());

            ServiceLocator<GameManager>.Register(new GameManager());
        }
    }
}