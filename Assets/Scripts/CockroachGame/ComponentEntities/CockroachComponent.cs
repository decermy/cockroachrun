using System;
using UnityEngine;

namespace CockroachGame.ComponentEntities
{
    public class CockroachComponent : MonoBehaviour
    {
        public event Action OnFinished = delegate { };

        [SerializeField]
        private Transform _headDirection;

        public Transform HeadDirection => _headDirection;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.TryGetComponent(out FinishZoneComponent finishZoneComponent))
            {
                OnFinished?.Invoke();
            }
        }
    }
}