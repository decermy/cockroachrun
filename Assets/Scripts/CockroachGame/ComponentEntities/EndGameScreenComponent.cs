using UnityEngine;
using UnityEngine.UI;

namespace CockroachGame.ComponentEntities
{
    public class EndGameScreenComponent : MonoBehaviour
    {
        [SerializeField]
        private Canvas _canvas;

        [SerializeField]
        private Button _tryAgainButton;
    
        public Canvas Canvas => _canvas;

        public Button TryAgainButton => _tryAgainButton;
    }
}