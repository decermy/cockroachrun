using System;
using CockroachGame.ComponentEntities;
using CockroachGame.GameObjectsManager;
using CockroachGame.Input;
using ServiceLocator;
using UnityEngine;

namespace CockroachGame
{
    public class DungerZoneController : IInitializable, IUpdatable
    {
        [Serializable]
        public class Settings
        {
            [Range(0, 10f)]
            public float DangerZoneRange = 3;
        }


        private IInputManager _inputManager;
        private DangerZoneComponent _dangerZoneComponent;
        private GameConfig _gameConfig;

        private Vector2 _oneV3 = Vector2.one;

        public void Initialize()
        {
            _inputManager = ServiceLocator<IInputManager>.Get();

            _dangerZoneComponent = ServiceLocator<IGameObjectManager>.Get().DangerZoneComponent;

            _gameConfig = ServiceLocator<GameConfig>.Get();
        }

        public void Update()
        {
            _dangerZoneComponent.transform.position = _inputManager.PointerPosition;

            _dangerZoneComponent.transform.localScale = _oneV3 * _gameConfig.DungerZoneSettings.DangerZoneRange;
        }
    }
}