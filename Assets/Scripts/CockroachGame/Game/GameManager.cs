using System;
using CockroachGame.Cockroach;
using CockroachGame.ComponentEntities;
using CockroachGame.GameObjectsManager;
using ServiceLocator;

namespace CockroachGame.Game
{
    public class GameManager : IInitializable, IDisposable
    {
        private CockroachComponent _cockroachComponent;
        private EndGameScreenComponent _endGameScreenComponent;
        private CockroachController _cockroachController;

        public void Initialize()
        {
            _cockroachController = ServiceLocator<CockroachController>.Get();

            _cockroachComponent = ServiceLocator<IGameObjectManager>.Get().CockroachComponent;

            _endGameScreenComponent = ServiceLocator<EndGameScreenComponent>.Get();

            _cockroachComponent.OnFinished += OnFinished;

            _endGameScreenComponent.TryAgainButton.onClick.AddListener(OnTryAgainClicked);
        }


        public void Dispose()
        {
            if (_cockroachComponent != null)
            {
                _cockroachComponent.OnFinished -= OnFinished;
            }

            if (_endGameScreenComponent && _endGameScreenComponent.TryAgainButton)
            {
                _endGameScreenComponent.TryAgainButton.onClick.RemoveListener(OnTryAgainClicked);
            }
        }

        private void OnFinished()
        {
            _endGameScreenComponent.Canvas.enabled = true;
        }

        private void OnTryAgainClicked()
        {
            _cockroachController.Reset();

            _endGameScreenComponent.Canvas.enabled = false;
        }
    }
}