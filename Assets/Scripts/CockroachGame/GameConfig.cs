using CockroachGame.Cockroach;
using UnityEngine;

namespace CockroachGame
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObjects/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [SerializeField]
        private CameraController.Settings _cameraControllerSettings;

        [SerializeField]
        private DungerZoneController.Settings _dungerZoneSettings;

        [SerializeField]
        private CockroachController.Settings _cockroachControllerSettings;

        public CameraController.Settings CameraControllerSettings => _cameraControllerSettings;

        public CockroachController.Settings CockroachControllerSettings => _cockroachControllerSettings;

        public DungerZoneController.Settings DungerZoneSettings => _dungerZoneSettings;
    }
}