using CockroachGame;
using CockroachGame.ComponentEntities;
using ServiceLocator;
using UnityEngine;

public class GameObjectFactory : IInitializable
{
    private PrefabsConfig _prefabsConfig;

    public void Initialize()
    {
        _prefabsConfig = ServiceLocator<PrefabsConfig>.Get();
    }

    public CockroachComponent CreateCockroachComponent()
    {
        return CreateGameObject(_prefabsConfig.CockroachComponent);
    }
    
    public DangerZoneComponent CreateDangerZoneComponent()
    {
        return CreateGameObject(_prefabsConfig.DangerZoneComponent);
    }

    private T CreateGameObject<T>(T monobehComponent) where T : MonoBehaviour
    {
        return MonoBehaviour.Instantiate(monobehComponent);
    }
}
