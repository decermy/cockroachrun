using CockroachGame.ComponentEntities;
using ServiceLocator;

namespace CockroachGame.GameObjectsManager
{
    public class GameObjectManager : IGameObjectManager, IInitializable
    {
        private CockroachComponent _cockroachComponent;
        private DangerZoneComponent _dangerZoneComponent;

        CockroachComponent IGameObjectManager.CockroachComponent => _cockroachComponent;
        DangerZoneComponent IGameObjectManager.DangerZoneComponent => _dangerZoneComponent;

        public void Initialize()
        {
            GameObjectFactory gameObjectFactory = ServiceLocator<GameObjectFactory>.Get();

            SetUpGameObjects(gameObjectFactory);
        }

        private void SetUpGameObjects(GameObjectFactory gameObjectFactory)
        {
            _cockroachComponent = gameObjectFactory.CreateCockroachComponent();
            _dangerZoneComponent = gameObjectFactory.CreateDangerZoneComponent();
        }
    }
}