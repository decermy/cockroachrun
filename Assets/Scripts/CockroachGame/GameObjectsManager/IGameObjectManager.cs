using CockroachGame.ComponentEntities;

namespace CockroachGame.GameObjectsManager
{
    public interface IGameObjectManager
    {
        CockroachComponent CockroachComponent { get; }
        DangerZoneComponent DangerZoneComponent { get; }
    }
}