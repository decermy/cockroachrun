using UnityEngine;

namespace CockroachGame.Input
{
    public interface IInputManager 
    {
        public Vector3 PointerPosition { get;}
        public bool IsGetPointer { get; }
        public bool IsGetPointerUp { get; }
        public bool IsGetPointerDown { get; }
    }
}
