using ServiceLocator;
using UnityEngine;

namespace CockroachGame.Input
{
    public class InputManager : IInputManager, IInitializable
    {
        private Camera _camera;
        private GameConfig _gameConfig;
        private Vector3 _forwardV3;

        Vector3 IInputManager.PointerPosition => GetWorldPosition();
        bool IInputManager.IsGetPointer => UnityEngine.Input.GetMouseButton(0);
        bool IInputManager.IsGetPointerUp => UnityEngine.Input.GetMouseButtonUp(0);
        bool IInputManager.IsGetPointerDown => UnityEngine.Input.GetMouseButtonDown(0);

        void IInitializable.Initialize()
        {
            _camera = ServiceLocator<Camera>.Get();
            _gameConfig = ServiceLocator<GameConfig>.Get();

            _forwardV3 = Vector3.forward;
        }

        public Vector3 GetWorldPosition()
        {
            var worldPosition =
                _camera.ScreenToWorldPoint(UnityEngine.Input.mousePosition + _forwardV3 * _gameConfig.CameraControllerSettings.CameraDistance);
            worldPosition.z = 0f;
            return worldPosition;
        }
    }
}