using CockroachGame.ComponentEntities;
using UnityEngine;

namespace CockroachGame
{
    [CreateAssetMenu(fileName = "PrefabsConfig", menuName = "ScriptableObjects/PrefabsConfig")]
    public class PrefabsConfig : ScriptableObject
    {
        [SerializeField]
        private CockroachComponent _cockroachComponent;
        
        [SerializeField]
        private DangerZoneComponent _dangerZoneComponent;

        public CockroachComponent CockroachComponent => _cockroachComponent;

        public DangerZoneComponent DangerZoneComponent => _dangerZoneComponent;
    }
}