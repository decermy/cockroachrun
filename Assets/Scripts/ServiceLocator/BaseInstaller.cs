﻿using System;
using UnityEngine;

namespace ServiceLocator
{
    public abstract class BaseInstaller : MonoBehaviour
    {
        private void Awake()
        {
            if (ServicesContainer.IsInstalled<BaseInstaller>())
                Destroy(this.gameObject);
            else
            {
                BaseInitialize();
                InstallServices();
                InitializeServices();
            }
        }

        private void OnDestroy()
        {
            DeinitializeServices();
        }

        private void BaseInitialize()
        {
            DontDestroyOnLoad(this.gameObject);
            var updatablesController = GetComponent<UpdatablesManager>();
            if (updatablesController == null)
                updatablesController = gameObject.AddComponent<UpdatablesManager>();
            ServiceLocator<UpdatablesManager>.Register(updatablesController);
            ServiceLocator<BaseInstaller>.Register(this);
        }

        protected abstract void InstallServices();

        private void InitializeServices()
        {
            ServicesContainer.InitializeAll();   
        }
        
        private void DeinitializeServices()
        {
            ServicesContainer.DeInitializeAll();   
        }
    }
}
