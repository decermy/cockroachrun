﻿using System;
using System.Collections.Generic;

namespace ServiceLocator
{
    public interface IInitializable
    {
        void Initialize();
    }

    public class ServiceLocator<T> where T : class
    {
        public static void Register(T instance)
        {
            ServicesContainer.Register(instance);
        }

        public static void Unregister()
        {
            ServicesContainer.Unregister<T>();
        }

        public static T Get()
        {
            return ServicesContainer.Get<T>();
        }
    }

    public static class ServicesContainer
    {
        private static Dictionary<Type, object> _instances = new Dictionary<Type, object>();

        public static void Register<T>(T instance) where T : class
        {
            _instances[typeof(T)] = instance;
        }

        public static void Unregister<T>()
        {
            _instances.Remove(typeof(T));
        }

        public static T Get<T>()
        {
            return (T) _instances[typeof(T)];
        }

        public static void InitializeAll()
        {
            UpdatablesManager updatablesManager = Get<UpdatablesManager>();
            foreach (KeyValuePair<Type, object> keyValuePair in _instances)
            {
                var instance = keyValuePair.Value;
                if (instance is IInitializable initializable)
                {
                    initializable.Initialize();
                }

                updatablesManager.TryAddInstance(instance);
            }
        }

        public static void DeInitializeAll()
        {
            UpdatablesManager updatablesManager = Get<UpdatablesManager>();
            foreach (KeyValuePair<Type, object> keyValuePair in _instances)
            {
                var instance = keyValuePair.Value;

                updatablesManager.TryRemoveInstance(instance);
            }

            foreach (KeyValuePair<Type, object> keyValuePair in _instances)
            {
                var instance = keyValuePair.Value;

                if (instance is IDisposable disposable)
                {
                    disposable.Dispose();
                }
            }
        }

        public static bool IsInstalled<T>()
        {
            return _instances.ContainsKey(typeof(T));
        }
    }
}