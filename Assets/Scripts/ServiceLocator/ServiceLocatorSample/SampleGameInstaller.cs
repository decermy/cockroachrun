﻿using UnityEngine;

namespace ServiceLocator
{
    public class SampleGameInstaller : BaseInstaller
    {
        [SerializeField] private Camera camera;

        //[SerializeField] private ScreenManager screenManager;
        protected override void InstallServices()
        {
            ServiceLocator<Camera>.Register(camera);
            //Service<IInputManager>.Register(new InputManager());
            //Service<GameManager>.Register(new GameManager());
            //Service<ScreenManager>.Register(screenManager);
        }
    }
}