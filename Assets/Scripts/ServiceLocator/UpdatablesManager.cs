using System;
using System.Collections.Generic;
using UnityEngine;

namespace ServiceLocator
{
    public interface IUpdatable
    {
        void Update();
    }

    public interface IFixedUpdatable
    {
        void FixedUpdate();
    }

    public interface ILateUpdatable
    {
        void LateUpdate();
    }

    public class UpdatablesManager : MonoBehaviour, IDisposable
    {
        private List<IUpdatable> _updatables = new List<IUpdatable>();
        private List<IFixedUpdatable> _fixedUpdatables = new List<IFixedUpdatable>();
        private List<ILateUpdatable> _lateUpdatables = new List<ILateUpdatable>();

        public void Update()
        {
            foreach (var updatable in _updatables)
            {
                updatable.Update();
            }
        }

        public void FixedUpdate()
        {
            foreach (var updatable in _fixedUpdatables)
            {
                updatable.FixedUpdate();
            }
        }

        public void LateUpdate()
        {
            foreach (var updatable in _lateUpdatables)
            {
                updatable.LateUpdate();
            }
        }

        public void TryAddInstance(object instance)
        {
            if (instance is IUpdatable updatable)
            {
                AddUpdatable(updatable);
            }

            if (instance is IFixedUpdatable fixedUpdatable)
            {
                AddUpdatable(fixedUpdatable);
            }

            if (instance is ILateUpdatable lateUpdatable)
            {
                AddUpdatable(lateUpdatable);
            }
        }

        public void TryRemoveInstance(object instance)
        {
            if (instance is IUpdatable updatable)
            {
                RemoveUpdatable(updatable);
            }

            if (instance is IFixedUpdatable fixedUpdatable)
            {
                RemoveUpdatable(fixedUpdatable);
            }

            if (instance is ILateUpdatable lateUpdatable)
            {
                RemoveUpdatable(lateUpdatable);
            }
        }

        private void AddUpdatable(IUpdatable updatable)
        {
            if (_updatables.Contains(updatable))
            {
                Debug.LogError($"IUpdatable already added");
                return;
            }

            _updatables.Add(updatable);
        }

        private void RemoveUpdatable(IUpdatable updatable)
        {
            if (_updatables.Contains(updatable) == false)
            {
                Debug.LogError($"IUpdatable missed");
                return;
            }
            
            _updatables.Remove(updatable);
        }

        private void AddUpdatable(IFixedUpdatable updatable)
        {
            if (_fixedUpdatables.Contains(updatable))
            {
                Debug.LogError($"IFixedUpdatable already added");
                return;
            }
            
            _fixedUpdatables.Add(updatable);
        }

        private void RemoveUpdatable(IFixedUpdatable updatable)
        {
            if (_fixedUpdatables.Contains(updatable) == false)
            {
                Debug.LogError($"IFixedUpdatable missed");
                return;
            }
            
            _fixedUpdatables.Remove(updatable);
        }

        private void AddUpdatable(ILateUpdatable updatable)
        {
            if (_lateUpdatables.Contains(updatable))
            {
                Debug.LogError($"ILateUpdatable already added");
                return;
            }
            
            _lateUpdatables.Add(updatable);
        }

        private void RemoveUpdatable(ILateUpdatable updatable)
        {
            if (_lateUpdatables.Contains(updatable) == false)
            {
                Debug.LogError($"ILateUpdatable missed");
                return;
            }
            
            _lateUpdatables.Remove(updatable);
        }

        public void Dispose()
        {
            _updatables.Clear();
            _fixedUpdatables.Clear();
            _lateUpdatables.Clear();
        }
    }
}